/**
 * Fichero: ModeloHashSet.java
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */

package modelo;

import java.util.HashSet;
import java.util.Iterator;

public class ModeloHashSet implements Modelo {

  HashSet alumnos = new HashSet();
  int id = 0;

  @Override
  public void create(Alumno alumno) {    
    alumnos.add(alumno);
    id++;
  }

  @Override
  public HashSet read() {    
    return alumnos;
  }

  @Override
  public void update(Alumno alumno) {
    Iterator it = alumnos.iterator();
    Alumno a;
        
    while (it.hasNext()) {
      a = (Alumno) it.next();
      if (a.getId().equals(alumno.getId())) {
        alumnos.remove(a);
        alumnos.add(alumno);
      }
    }
  }

  @Override
  public void delete(Alumno alumno) {
    Iterator it = alumnos.iterator();
    Alumno a;
   
    while (it.hasNext()) {
      a = (Alumno) it.next();
      if (a.getId().equals(alumno.getId())) {
        alumnos.remove(a);

      }
    }
  }

  @Override
  public int getId() {    
    return id;
  }
}