/**
 * Fichero: Modelo.java
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */
package modelo;

import java.util.HashSet;

public interface Modelo {

    public void create(Alumno alumno); // Crea un alumno nuevo

    public HashSet read(); // Muestra los alunnos.

    public void update(Alumno alumno); // Actuzaliza el alumno.

    public void delete(Alumno alumno);  // Borrar el alunno con el id dado

    public int getId(); // Obtiene el último id dado. Usado para create.
}