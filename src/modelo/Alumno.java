/**
 * Fichero: Alumno.java
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */

package modelo;

public class Alumno {

  private String id;
  private String nombre;
  private int edad;
  private String email;

  public Alumno(String id_, String nombre_, int edad_, String email_) {    
    id = id_;
    nombre = nombre_;
    edad = edad_;
    email = email_;
  }

  public Alumno() {
    nombre = "";
    edad = 0;
  }  
  
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public int getEdad() {
    return edad;
  }

  public void setEdad(int edad) {
    this.edad = edad;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}