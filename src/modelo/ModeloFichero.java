/**
 * Fichero: ModeloFichero.java
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */
package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;

public class ModeloFichero implements Modelo {

  String fichero = "1415ceed11prgt6e1.csv";
  int Id = 0;

  @Override
  public void create(Alumno alumno) {  // Muchísimas gracias por su orientación
                                       // Aquí he aprendido mucho...
    try {
      FileWriter fw = new FileWriter(fichero, true);

      fw.write(alumno.getId());
      fw.write(";");
      fw.write(alumno.getNombre());
      fw.write(";");
      String age = Integer.toString(alumno.getEdad());
      fw.write(age);
      fw.write(";");
      fw.write(alumno.getEmail());
      fw.write("\r\n");

      if (fw != null) {
        fw.flush(); //Para limpiar canal
        fw.close();
        Id++;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public HashSet read() {

    HashSet hs = new HashSet();
    Alumno alumno = new Alumno();
    File fe = new File(fichero);
    if (fe.exists()) {
      try {
        FileReader fr = new FileReader(fe);
        BufferedReader br = new BufferedReader(fr);
        String linea;
        while ((linea = br.readLine()) != null) {
          alumno = extraeAlumno(linea);
          hs.add(alumno);
        }
        if (fr != null) {
        fr.close();          
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return hs;
  }

  @Override
  public void update(Alumno alumno) { 
    
    // Esta parte, y gracias al desarrollo de "Delete" y "Update"
    // funciona perfectamente. aquí me ha quedado claro lo que significa "this"
    
    this.delete(alumno);
    this.create(alumno);

  }

  @Override
  public void delete(Alumno alumno) { // ...Pero en esta fase es la que más he aprendido
                                      // a base de errores variados y muchas horas.
HashSet hs = new HashSet();
Iterator it = hs.iterator();
Alumno a;

String temporal ="temp.cvs";
File fe = new File(fichero);
File tmp = new File(temporal);
    if (fe.exists()) {
      try {
        FileReader fr = new FileReader(fe);
        BufferedReader br = new BufferedReader(fr);
        String linea;
        
        FileWriter fw = new FileWriter(temporal, true);
        
        while ((linea = br.readLine()) != null) {
          
         a = extraeAlumno(linea);           
         if (!a.getId().equals(alumno.getId())) {
         hs.add(a);
         
      fw.write(a.getId());
      fw.write(";");
      fw.write(a.getNombre());
      fw.write(";");
      String age = Integer.toString(a.getEdad());
      fw.write(age);
      fw.write(";");
      fw.write(a.getEmail());
      fw.write("\r\n");       
          
        if (fw != null) {
        fw.flush(); //Para limpiar canal
        fw.close();         
        }                   
      } else {hs.remove(a);
    }  
  }
        if (fr != null) {
          fr.close(); 
 
          // borrado del fichero principal
          //y renombrado del temporal actualizado que pasa a ser principal
          
         fe.delete();
         boolean flag = tmp.renameTo(fe); // No encontré una forma mejor de hacer esto.
          
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }    
  }

  @Override
  public int getId() {
    return Id;

  }

  private Alumno extraeAlumno(String linea) {
    Alumno a;

    StringTokenizer str = new StringTokenizer(linea, ";");

    String id = str.nextToken();
    String nombre = str.nextToken();
    String edad_ = str.nextToken();
    int edad = Integer.parseInt(edad_);
    String email = str.nextToken();

    a = new Alumno(id, nombre, edad, email);

    return a;
  }
}