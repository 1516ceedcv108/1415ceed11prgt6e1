/**
 * Fichero: ModeloArrayList.java
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */

package modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class ModeloArrayList implements Modelo {

  ArrayList<Alumno> alumnos = new ArrayList<>();
  int id = 0;

  @Override
  public void create(Alumno alumno) {    
    alumnos.add(alumno);
    id++;
  }

    @Override
    public HashSet read() {      
        HashSet hs = new HashSet();
        Iterator it = alumnos.iterator();
        Alumno a;

        while (it.hasNext()) {
            a = (Alumno) it.next();
            hs.add(a);
        }
    return hs;
    }

  @Override
  public void update(Alumno alumno) {
    Iterator it = alumnos.iterator();
    Alumno a;
    int pos = 0;

    while (it.hasNext()) {
      a = (Alumno) it.next();
      if (a.getId().equals(alumno.getId())) {
        alumnos.set(pos, alumno);
      }
    }
  }

  @Override
  public void delete(Alumno alumno) {
    Iterator it = alumnos.iterator();
    Alumno a;
    int position = 0;

    while (it.hasNext()) {
      a = (Alumno) it.next();
      if (a.getId().equals(alumno.getId())) {
        alumnos.remove(a);
      }
      position++;
    }
  }

  @Override
  public int getId() {    
    return id;    
  }
}