/**
 * Fichero: Main.java
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */

package controlador;

import java.io.IOException;
import modelo.Modelo;
import vista.Vista;


public class Main {

  public static void main(String[] args) throws IOException {
      
    Modelo m = null;
    Vista v = new Vista();
    Controlador c = new Controlador(m, v);
  }
}