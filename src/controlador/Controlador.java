/**
 * Fichero: Controlador.java
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */
package controlador;

import java.io.IOException;
import java.util.HashSet;
import modelo.*;
import vista.Vista;

class Controlador {

    private Modelo modelo;
    private final Vista vista;
    private final char opcion;

    Controlador(Modelo modelo_, Vista vista_) throws IOException {
        modelo = modelo_;
        vista = vista_;
        opcion = menuModelo();
        if (opcion != 'e') {
            menucrud();
        }
    }

    private void menucrud() throws IOException {
        String linea;
        char option = ' ';
        String id; // Posicion a actualziar
        Alumno alumno;

        do {

            option = vista.menu();

            option = toMinuscula(option);

            switch (option) {

                case 'e': // Exit
                    vista.exit();
                    break;

                case 'c': // Crear/AñadirAñadir
                    alumno = vista.getAlumno();
                    id = Integer.toString(modelo.getId() + 1);
                    alumno.setId(id);
                    modelo.create(alumno);
                    break;
                  
                case 'r': // Read / Obtener / Listar
                    modelo.read();
                    HashSet hs = new HashSet();
                    hs = modelo.read();
                    vista.showAlumnos(hs);
                    break;
                  
                case 'u':  // Actualizar
                    id = vista.getId();
                    alumno = vista.getAlumno();
                    alumno.setId(id);
                    modelo.update(alumno);
                    break;
                  
                case 'd': // Borrar
                    id = vista.getId();
                    alumno = new Alumno(id, "", 0, "");
                    modelo.delete(alumno);
                    break;
                  
                default:
                    vista.error();
                    break;
            }

        } while (option != 'e');

    }

    public static char toMinuscula(char opcion) {
        String linea;
        linea = opcion + "";
        linea = linea.toLowerCase();
        opcion = linea.charAt(0);
        return opcion;
    }

    private char menuModelo() throws IOException {

        String linea;
        char opcion = ' ';
        String id; // Posicion a actualziar
        Alumno alumno;

        opcion = vista.menuModelos();

        opcion = toMinuscula(opcion);
              
        switch (opcion) {

            case 'e': // Exit
                vista.exit();
                break;

            case 'v':
                modelo = new ModeloVector();
                break;
            case 'a':
                modelo = new ModeloArrayList();
                break;
            case 's':
                modelo = new ModeloHashSet();
                break;
            case 'f':
                modelo = new ModeloFichero();
                break;
            default:
                vista.error();
                break;                
      }
       return opcion;       
    }      
  }