/**
 * Fichero: Vista.java
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 01-02-2015
 */
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Alumno;

public class Vista {

    public static boolean validarEmail(String s) {
        Pattern p = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    public Alumno getAlumno() {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        Alumno a;
        String nombre = "";
        String email = "";
        String linea = "";
        int edad = 0;
        boolean error = true;
        boolean esvalido = true;
        String id;

        System.out.println("ENTRADA DE DATOS");

        System.out.print("Nombre: ");
        try {
            nombre = br.readLine();
        } catch (IOException ex) {
        }

        while (error == true) {

            System.out.print("Edad: ");
            try {
                linea = br.readLine();
                edad = Integer.parseInt(linea);
                error = false;
            } catch (IOException ex) {
            } catch (NumberFormatException nfe) {
                System.out.println("Error: Se debe introducir un número");
                error = true;
            }
        }

        error = true;
        while (error == true) {

            System.out.print("Email: ");
            try {
                email = br.readLine();
                esvalido = validarEmail(email);
                if (esvalido) {
                    error = false;
                } else {
                    System.out.println("Error: Email no valido");
                    error = true;
                }
            } catch (IOException ex) {
            }
        }
        a = new Alumno("", nombre, edad, email);
        return a;
    }

    public void readAlumno(Alumno alumno) {

        System.out.println("Id: " + alumno.getId());
        System.out.println("Nombre: " + alumno.getNombre());
        System.out.println("Edad: " + alumno.getEdad());
        System.out.println("Email: " + alumno.getEmail());

    }

    public char menu() throws IOException {

        char opcion = ' ';

        System.out.println("MENU CRUD ");
        System.out.println("e. exit ");
        System.out.println("c. create ");
        System.out.println("r. read ");
        System.out.println("u. update ");
        System.out.println("d. delete ");
        System.out.print("Opción:  ");
        opcion = LeerCaracter();

        return opcion;
    }

    private char LeerCaracter() throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        char c = ' ';
        c = (char) isr.read();
        return c;
    }

    public void exit() {
        System.out.println("Fin ");
    }

    public String getId() throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        char c = ' ';
        String linea = "";

        System.out.print("Id: ");
        linea = br.readLine();

        return linea;

    }

    public void error() {
        System.out.println("Error: Opcion Incorrecta ");
    }

    public char menuModelos() throws IOException {

        char opcion = ' ';

        System.out.println("MENU ESTRUCTURA ");
        System.out.println("e. exit ");
        System.out.println("v. vector ");
        System.out.println("a. arraylist ");
        System.out.println("s. hashset ");
        System.out.println("f. fichero ");
        System.out.print("Opción:  ");
        opcion = LeerCaracter();

        return opcion;
    }

    public void showAlumnos(HashSet hs) {
        Vista vst = new Vista();
        Iterator it = hs.iterator();
        Alumno alumno = null;

        System.out.println("DATOS ALUMNOS");
        while (it.hasNext()) {
            alumno = (Alumno) it.next();
            vst.readAlumno(alumno);
        }
    }
}